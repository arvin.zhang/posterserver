package net.zb.examination.poster;

import com.alibaba.fastjson.JSON;
import net.zb.examination.poster.tools.FileTools;
import net.zb.examination.poster.tools.HttpTools;
import net.zb.examination.poster.vo.SearchResult;

import java.io.IOException;

/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/19, 初始化版本
 * @version 1.0
 **/
public class PostMain {




	public static void main(String[] args) throws IOException {
		String result = HttpTools.sengGet("1");
		SearchResult searchResult = JSON.parseObject(result, SearchResult.class);
		FileTools.downloadFile(searchResult.getPoster(), "test1.jpg");
		System.out.println(result);
	}

}
