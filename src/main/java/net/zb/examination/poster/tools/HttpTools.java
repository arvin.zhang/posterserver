package net.zb.examination.poster.tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/19, 初始化版本
 * @version 1.0
 **/
public class HttpTools {


	private static final String URL_STR = "http://www.omdbapi.com";
	private static final String APIKEY = "8e142584";

	public static String sengGet(String t) throws IOException {
		BufferedReader in = null;
		StringBuilder result= new StringBuilder();
		try {
			URL url = new URL(URL_STR + "?t=" + t + "&apikey=" + APIKEY);
			URLConnection connection = url.openConnection();
			connection.setRequestProperty("accept", "*/*");
			connection.setRequestProperty("connection", "Keep-Alive");
			connection.setConnectTimeout(2000);
			connection.connect();
			Map<String, List<String>> map = connection.getHeaderFields();
			// 遍历所有的响应头字段
			for (String key : map.keySet()) {
				System.out.println(key + "--->" + map.get(key));
			}
			// 定义 BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				result.append(line);
			}
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return result.toString();
	}
}
