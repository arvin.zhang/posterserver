package net.zb.examination.poster.tools;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;

/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/19, 初始化版本
 * @version 1.0
 **/
public class FileTools {




	public static void downloadFile(String url, String fileName){
		try {
			URL weburl = new URL(url);
			URLConnection con = weburl.openConnection();
			con.setConnectTimeout(5 * 1000);
			InputStream is = con.getInputStream();
			byte[] bs = new byte[1024];
			int len;
			OutputStream os = new FileOutputStream(fileName);
			while ((len = is.read(bs)) != -1) {
				os.write(bs, 0, len);
			}
			os.close();
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
